This is a test Junit program that we are going to be adding version control to and we will be following SQM plan guidelines.

Tests are run through the command line by navigating to this directory, then running the command "mvn test".

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=thorir14_verkefni1&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=thorir14_verkefni1)
